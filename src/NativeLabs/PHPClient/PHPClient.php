<?php namespace NativeLabs\PHPClient;

include("PHPClientException.php");
include("PHPClientCore.php");

class PHPClient extends PHPClientCore {

    const VERSION = '1.0';
    const IPV4    = 'IPv4';
    const IPV6    = 'IPv6';

    protected $base = "http://localhost:8000/api/v1/";


    /**
     * Constructor.
     *
     * @param int    $id
     * @param string $secret
     */
    public function __construct($id, $secret)
    {
        parent::__construct($id, $secret);
    }


    /**
     * Get list of routers
     *
     * @param  array $constraints
     * @return array
     */
    public function getRouters($constraints=[])
    {
        return $this->getRessource("routers", $constraints);
    }

    /**
     * Get a router by ID
     *
     * @param  array $constraints
     * @return array
     */
    public function getRouter($id)
    {
        return $this->getRessource("routers", [], $id);
    }

    /**
     * Add a router
     *
     * @param  array $data
     * @return array
     */
    public function addRouter($data)
    {
        return $this->postRessource("routers", $data);
    }

    /**
     * Edit a router
     *
     * @param  int   $id
     * @param  array $data
     * @return array
     */
    public function editRouter($id, $data)
    {
        return $this->putRessource("routers", $id, $data);
    }

    /**
     * delete a router
     *
     * @param  int   $id
     * @return array
     */
    public function deleteRouter($id)
    {
        return $this->deleteRessource("routers", $id);
    }


    /**
     * Get list of networks
     *
     * @param  array $constraints
     * @return array
     */
    public function getNetworks($constraints=[])
    {
        return $this->getRessource("networks", $constraints);
    }

    /**
     * Get a network by ID
     *
     * @param  array $constraints
     * @return array
     */
    public function getNetwork($id)
    {
        return $this->getRessource("networks", [], $id);
    }

    /**
     * Add a network
     *
     * @param  array $data
     * @return array
     */
    public function addNetwork($data)
    {
        return $this->postRessource("networks", $data);
    }


    /**
     * Delete a network
     *
     * @param  int   $id
     * @return array
     */
    public function deleteNetworks($id)
    {
        return $this->deleteRessource("networks", $id);
    }

    /**
     * Get list of transactions
     *
     * @param  array $constraints
     * @return array
     */
    public function getTransactions($constraints=[])
    {
        return $this->getRessource("transactions", $constraints);
    }

    /**
     * Get a transaction
     *
     * @param  int $id
     * @return array
     */
    public function getTransaction($id)
    {
        return $this->getRessource("transactions", [], $id);
    }

    /**
     * Get list of stats
     *
     * @param  array $constraints
     * @return array
     */
    public function getStats($constraints=[])
    {
        return $this->getRessource("stats", $constraints);
    }

    /**
     * Get a stat
     *
     * @param  int $id
     * @return array
     */
    public function getStat($id)
    {
        return $this->getRessource("stats", [], $id);
    }


    /**
     * Get ressource
     *
     * @param string $ressource
     * @param array $constraints
     * @return array
     */
    private function getRessource($ressource, $constraints=[], $id=null)
    {
        return $this->sendRequest("GET", $this->buildUrl($ressource, $constraints, $id), date(DATE_RSS));
    }

    /**
     * Post ressource
     *
     * @param string $ressource
     * @param array  $data
     * @return array
     */
    private function postRessource($ressource, $data)
    {
        return $this->sendRequest("POST", $this->buildUrl($ressource), date(DATE_RSS), $data);
    }

    /**
     * Put ressource
     *
     * @param  string $ressource
     * @param  int    $id
     * @param  array  $data
     * @return array
     */
    private function putRessource($ressource, $id, $data)
    {
        return $this->sendRequest("PUT", $this->buildUrl($ressource, [], $id), date(DATE_RSS), $data);
    }

    /**
     * Delete ressource
     *
     * @param  string $ressource
     * @param  int    $id
     * @return array
     */
    private function deleteRessource($ressource, $id)
    {
        return $this->sendRequest("DELETE", $this->buildUrl($ressource, [], $id), date(DATE_RSS));
    }

    /**
     * Build url with constraints
     *
     * @param  string $ressource
     * @param  array  $constraints
     * @return string
     */
    private function buildUrl($ressource, $constraints=[], $id=null)
    {
        // Build basic ressource url
        $url = $this->base.$ressource;

        // If id is given concatenate it
        if ($id !== null) {
            $url .= "/$id";
        }

        // Prepare for concatenation with GET params
        $url .= "?";

        // Add GET params
        if (array_key_exists("limit", $constraints)) {
            $url .= "limit=".$constraints["limit"]."&";
        }
        if (array_key_exists("offset", $constraints)) {
            $url .= "offset=".$constraints["offset"]."&";
        }
        if (array_key_exists("fields", $constraints)) {
            if (is_array($constraints["fields"])) {
                $url .= "fields=".implode(',', $constraints["fields"]);
            } else {
                throw new PHPClientException("Fields must be an array");
            }
        }

        // Trim last ? or &
        $url = rtrim($url, "?&/");

        return $url;
    }
}

// $a = new PHPClient(3, "yG0wLFXRiI7WB0f2JM4eFVm7u2qipdSK");
// // $res = $a->editRouter(5, ["type"=>"IPv4","address"=>"8.9.8.9","manufacturer"=>1]);
// $res = $a->getRouters();
// // $res = $a->getRouter(4);
// var_dump($res);
