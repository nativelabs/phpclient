<?php namespace NativeLabs\PHPClient;


class PHPClientCore {

    /**
     * Client ID.
     *
     * @var int
     */
    protected $id;

    /**
     * API secret key.
     *
     * @var string
     */
    protected $secret;

    protected $type = "application/json";


    /**
     * Constructor.
     *
     * @param  int    $id
     * @param  string $secret
    */
    public function __construct($id, $secret)
    {
        if (!is_int($id)) {
            throw new PHPClientException("ID is an integer");
        }
        if (strlen($secret) < 32) {
            throw new PHPClientException("Secret key is a 32-character string");
        }

        $this->secret = $secret;
        $this->id = $id;
    }


    /**
     * Send request
     *
     * @param string $methode
     * @param string $date
     * @param string $url
     * @param array $payload
     * @return string
     */
    public function sendRequest($methode, $url, $date, $payload=[])
    {
        // We first get the signature
        $md5 = md5(json_encode($payload));
        $signature = $this->getSignature($methode, $date, $md5, $this->type, $url);

        // Init curl
        $curl = curl_init();

        // Then set the headers and contents
        $this->prepareRequest($curl, $methode, $date, $md5, $this->type, $url, $signature, $payload);

        // Send the request
        $result = $this->execRequest($curl);

        // Handle errors
        $this->handleErrors($curl, $result);

        // Close connection
        curl_close($curl);

        return $result;
    }


    /**
     * Calculate signature of a request
     *
     * @param string $methode
     * @param string $date
     * @param string $md5
     * @param string $type
     * @param string $url
     * @return string
     */
    private function getSignature($methode, $date, $md5, $type, $url)
    {
        // We first concatenate all params
        $string = "$methode\n$type\n$md5\n$date\n$url";

        // Now we get the sha256 of the string using the key and apply base64
        $signature = base64_encode(hash_hmac("sha256", $string, $this->secret));
        // echo($string."\n\n");
        return $signature;
    }

    /**
     * Set headers and contents
     *
     * @param object $curl
     * @param string $methode
     * @param string $date
     * @param string $md5
     * @param string $type
     * @param string $url
     * @param string $signature
     * @param array  $payload
     */
    private function prepareRequest($curl, $methode, $date, $md5, $type, $url, $signature, $payload)
    {
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => true,
            CURLINFO_HEADER_OUT => true,
            CURLOPT_URL => $url,
            CURLOPT_HTTPHEADER => [
                "Authorization: NativeLabs ".$this->id.":$signature",
                "Date: $date",
                "Content-MD5: $md5",
                "Content-Type: $type",
            ],
            CURLOPT_POSTFIELDS => json_encode($payload)
        ]);

        // Set methode
        if ($methode === "GET") {
            curl_setopt($curl, CURLOPT_HTTPGET, true);
        }
        if ($methode === "POST") {
            curl_setopt($curl, CURLOPT_POST, count($payload));
        }
        if ($methode === "PUT") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
        }
        if ($methode === "DELETE") {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
    }

    /**
     * Calculate signature of a request
     *
     * @param object $curl
     * @return object
     */
    private function execRequest($curl)
    {
        // Send request and decode result
        $result = curl_exec($curl);
        echo $result."\n\n";
        $result = json_decode($result);

        return $result;
    }

    /**
     * Handle errors
     *
     * @param object $result
     */
    private function handleErrors($curl, $result)
    {
        $status = (int) curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if ($status !== 200) {
            if (is_object($result)) throw new PHPClientException(json_encode($result));
            else throw new PHPClientException("Null returned");
        }
    }
}
